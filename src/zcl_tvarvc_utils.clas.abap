class ZCL_TVARVC_UTILS definition
  public
  final
  create public .

public section.

  types:
    BEGIN OF ty_rt_tab,
      sign   TYPE tvarvc-sign,
      option TYPE tvarvc-opti,
      low    TYPE tvarvc-low,
      high   TYPE tvarvc-high,
    END OF ty_rt_tab .
  types TTY_RT_TAB TYPE STANDARD TABLE OF ty_rt_tab WITH DEFAULT KEY.
protected section.
private section.

  class-methods GET_RANGE_TAB
    importing
      !IM_VAR_NAME type TVARVC-NAME
      !IM_RANGE_TAB_TYPE type STRING
      !IM_SELECT_CAT type TVARVC-TYPE optional
      !IM_NUMBER type TVARVC-NUMB optional
    returning
      value(RT_RANGE_TAB) type ref to DATA
    exceptions
      TYPE_NOT_FOUND .
ENDCLASS.



CLASS ZCL_TVARVC_UTILS IMPLEMENTATION.


  METHOD get_range_tab.
    DATA lv_sel_type TYPE tvarvc-type.
    FIELD-SYMBOLS <fs_tab> TYPE table.

    " Check if type imported is valid
    DATA(lo_elem) = cl_abap_typedescr=>describe_by_name( im_range_tab_type ).
    IF sy-subrc <> 0.
      RAISE type_not_found.
    ENDIF.

    " Create data rt_range_tab at runtime
    CREATE DATA rt_range_tab TYPE TABLE OF (im_range_tab_type).

    " Check for type of selection
    IF im_select_cat IS NOT INITIAL.
      lv_sel_type = im_select_cat.
    ELSE.
      lv_sel_type = 'S'.
    ENDIF.

    " Fill range tab from tvarvc
    SELECT sign opti low high FROM tvarvc INTO TABLE <fs_tab>
       WHERE name = im_var_name
       AND type = lv_sel_type.

  ENDMETHOD.
ENDCLASS.

class ZCL_SELSCREEN_HELPER definition
  public
  final
  create public .

public section.

  types:
    BEGIN OF ty_struct,
             row_name TYPE string,
             row_type TYPE string,
           END OF ty_struct .
  types:
    t_out TYPE STANDARD TABLE OF ty_struct WITH DEFAULT KEY .

  methods GET_ITAB_OF_PARAMS
    importing
      !IM_CPROG type CPROG default 'sy-cprog'
    returning
      value(RT_TAB_PARAMS) type T_OUT .
  PROTECTED SECTION.
  PRIVATE SECTION.
    DATA gt_rsscr TYPE TABLE OF rsscr WITH DEFAULT KEY. " Structure of itab %_SSCR (selection screen,ABAP/4)

ENDCLASS.



CLASS ZCL_SELSCREEN_HELPER IMPLEMENTATION.


  METHOD get_itab_of_params.
    DATA ls_struct TYPE ty_struct.
    DATA elm_name TYPE string.

    LOAD REPORT sy-cprog PART 'SSCR' INTO gt_rsscr.
    DELETE gt_rsscr WHERE kind <> 'S' AND kind <> 'P'. " Take only select-options and parameters
    CHECK lines( gt_rsscr ) > 0.
    LOOP AT gt_rsscr ASSIGNING FIELD-SYMBOL(<gs_rsscr>).
      elm_name = SWITCH #( <gs_rsscr>-kind
                           WHEN 'S' THEN |{ <gs_rsscr>-name }[]|
                           WHEN 'P' THEN <gs_rsscr>-name
                         ).

      DATA(elm_type) = <gs_rsscr>-dbfield.
      ls_struct-row_name = elm_name.
      ls_struct-row_type = elm_type.
      APPEND ls_struct TO rt_tab_params.
      CLEAR ls_struct.
    ENDLOOP.
  ENDMETHOD.
ENDCLASS.

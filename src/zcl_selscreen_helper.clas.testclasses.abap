*"* use this source file for your ABAP unit test classes


CLASS zcl_selscreen_helper_test DEFINITION FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS
.
*?﻿<asx:abap xmlns:asx="http://www.sap.com/abapxml" version="1.0">
*?<asx:values>
*?<TESTCLASS_OPTIONS>
*?<TEST_CLASS>zcl_Selscreen_Helper_Test
*?</TEST_CLASS>
*?<TEST_MEMBER>f_Cut
*?</TEST_MEMBER>
*?<OBJECT_UNDER_TEST>ZCL_SELSCREEN_HELPER
*?</OBJECT_UNDER_TEST>
*?<OBJECT_IS_LOCAL/>
*?<GENERATE_FIXTURE>X
*?</GENERATE_FIXTURE>
*?<GENERATE_CLASS_FIXTURE/>
*?<GENERATE_INVOCATION/>
*?<GENERATE_ASSERT_EQUAL/>
*?</TESTCLASS_OPTIONS>v
*?</asx:values>
*?</asx:abap>
  PRIVATE SECTION.
    DATA:
      f_cut TYPE REF TO zcl_selscreen_helper.  "class under test

    METHODS: setup.
    METHODS: teardown.
    METHODS: get_itab_of_params FOR TESTING.
ENDCLASS.       "zcl_Selscreen_Helper_Test


CLASS zcl_selscreen_helper_test IMPLEMENTATION.

  METHOD setup.
    CREATE OBJECT f_cut.
  ENDMETHOD.


  METHOD teardown.
    CLEAR f_cut.
  ENDMETHOD.


  METHOD get_itab_of_params.

  ENDMETHOD.




ENDCLASS.

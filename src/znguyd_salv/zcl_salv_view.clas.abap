class ZCL_SALV_VIEW definition
  public
  final
  create public .

public section.

  data O_ALV_GRID type ref to CL_SALV_TABLE .
  data O_CONTROLLER type ref to ZCL_SALV_CONTROLLER .
protected section.
private section.

  data O_AGGREGATIONS type ref to CL_SALV_AGGREGATIONS .
  data O_COLUMN type ref to CL_SALV_COLUMN_TABLE .
  data O_COLUMNS type ref to CL_SALV_COLUMNS_TABLE .
  data O_EVENTS type ref to CL_SALV_EVENTS_TABLE .
  data O_FUNCTIONS type ref to CL_SALV_FUNCTIONS_LIST .
  data O_LAYOUT type ref to CL_SALV_LAYOUT .
  data O_SELECTIONS type ref to CL_SALV_SELECTIONS .
  data O_SORTS type ref to CL_SALV_SORTS .
  data O_SETTINGS type ref to CL_SALV_DISPLAY_SETTINGS .

  methods DISPLAY_BASIC_TOOLBAR .
ENDCLASS.



CLASS ZCL_SALV_VIEW IMPLEMENTATION.


  method DISPLAY_BASIC_TOOLBAR.
    o_functions = o_alv_grid->get_functions( ).
    o_functions->set_all( if_salv_c_bool_sap=>true ).
  endmethod.
ENDCLASS.

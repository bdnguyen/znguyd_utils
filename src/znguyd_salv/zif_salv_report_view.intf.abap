interface ZIF_SALV_REPORT_VIEW
  public .


  methods INITIALIZE
    importing
      !IV_REPORT_NAME type SY-REPID optional
      !IV_VARIANT type DISVARIANT-VARIANT optional
      !IO_CONTAINER type ref to CL_GUI_CONTAINER optional
      !IT_USER_COMMANDS type TTB_BUTTON optional
    changing
      !CT_DATA_TABLE type ANY TABLE optional .
  methods PREPARE_DISPLAY_DATA .
  methods DISPLAY .
  methods SET_COLUMN_ATTRIBUTES .
  methods ADD_SORT_CRITERIA .
  methods OPTIMISE_COLUMN_WIDTHS .
  methods SET_LIST_HEADER .
  methods REFRESH_DIPLAY .
endinterface.
